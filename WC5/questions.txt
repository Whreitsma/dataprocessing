Assemblix heeft 160 CPU's, 2 threads per core en 20 cores per socket. Dus 4 * 20 = 80 cores totaal
80 cores en 160 threads

Helix heeft 32 CPU's, 2 threads per core en 8 cores per socket. Dus 2 * 8 = 16 cores totaal
16 cores en 32 threads

Asterix heeft 16 CPU's, 2 threads per core en 4 cores per socket. Dus 2 * 4 = 8 cores totaal
8 cores en 16 threads

Mordor heeft 40 CPU's, 2 threads per core en 10 cores per socket. Dus 2 * 10 = 20 cores totaal
20 cores 40 threads

Een bin machine(Bin300) heeft 4 CPU's, 1 thread per core en 4 cores per socket,
Dus 4 cores en 4 threads

Mijn laptop heeft 12 CPU's, 2 threads per core en 6 cores per socket. Dus 1 * 6 = 6 cores totaal
6 cores 12 threads

Nuc 410 heeft 4 CPU's, 2 threads per core en 2 cores per socket. Dus 1 * 2 = 2 cores totaal
2 cores 4 threads


Wat is het verschil tussen bin 201 en nuc410?
whreitsma@bin201:~$ lscpu | egrep 'Thread|Core|Socket|^CPU\('
CPU(s):                          4
Thread(s) per core:              1
Core(s) per socket:              4
Socket(s):                       1

Nuc410 heeft evenveel CPU's als bin201, alleen heeft bin201 4 logische kernen in tegenstelling tot de 2 logische keren van nuc 410, maar omdat nuc410 hyperthreaded is, heeft het 2 threads per core in plaats van 1 in bin201.


Waarom de persoon verschillende hoeveelheden threads gebruikt per programma?
- omdat het ene programma sneller draait op minder cores dan het andere programma, dit komt doordat als je teveel cores voor een kleine opdracht hebt, je een 'bottleneck' gaat krijgen door de overhead van het aantal threads dat je gebruikt.

Een ding dat deze persoon kan verbeteren aan zijn code is het toevoegen van een thread variabele. Omdat als je nu een machine gaat gebruiken die minder threads heeft dan wat er hardcoded in de code staat hij een error gaat geven.




