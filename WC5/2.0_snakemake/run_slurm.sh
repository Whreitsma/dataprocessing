#!/bin/bash
source venv/bin/activate
snakemake --cluster "sbatch  -t {cluster.time} -N {cluster.nodes}" --cluster-config cluster_config.yaml --jobs 1 --latency-wait 30 --snakefile Snakefile
deactivate

