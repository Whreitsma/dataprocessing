# SNP calling pipeline

This pipeline processes double strains WGS fastq files and calls variants based on refrence genome.


### Requirements
This pipeline uses these tools for the conversion/calling.
- BWA
- samtools
- picard
- bcftools
- R (vcfR and optparse)
- python venv

## Setup

Create python virtual enviroment
```
$python3 -m venv {path/to/new/venv}
```
Activate python virtual environment
```
$source {path/to/venv}/bin/activate
```
Install Snakemake into virtual environment
```
pip3 install snakemake
```
Deactivate virtual environment
```
deactivate
```

## Running the pipeline
The pipeline can be customized with the use of *config.yaml*.
the changeable fields are shown in the table below:

| Name       | Description                                                                   |
|------------|-------------------------------------------------------------------------------|
| workdir    | String containing the path to the dataset                                     |
| Samples    | List containing the suffix to differntiate between forward and reverse strand |
| samplename | String containing the identifier(name) of the sample file                     |
| index      | Path to were the index will be stored and loaded.                             |
| reference  | Path to the reference file.                                                   |
| gff        | Path to the gff file                                                          |
| threads    | change the amount of threads used(I think it's deprecated 2 years later...)   |

After setting up the *config.yaml*, the pipeline can be called from the comandline with the following command:
```
$snakemake --snakefile Snakefile --cores {corecount}
```

Dag of the pipeline:

![](dag.png)





For the sake of testing, the dataset has been aligned to two references:
- Escherichia coli str. K-12 substr.
- Escherichia coli str. K-53 substr.

The dataset self is of the K-53 sub-strand, so it will not have that many snp's.
But it will show alot of variants with the K-12 sub-strain.

